import React from 'react';
import {View, StyleSheet,SafeAreaView} from 'react-native';
import HomeScreen from './Source/Screens/Dashboard/MyForm';
import Calender from './Source/Screens/Dashboard/Calender';
import Router from './Source/Router/index';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Router />
        {/* <HomeScreen></HomeScreen> */}
        {/* <Calender></Calender> */}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
