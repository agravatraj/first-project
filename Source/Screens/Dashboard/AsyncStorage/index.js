/* eslint-disable eqeqeq */
/* eslint-disable no-undef */
import React from 'react';
import {
  Text,
  TextInput,
  ActivityIndicator,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import styles from './styles';
import MyButton from '../../../Component/CustomeButton';
import AsyncStorage from '@react-native-community/async-storage';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      TextInputValue: '',
      DisplayValue: '',
    };
  }

  storeData = async () => {
    try {
      await AsyncStorage.setItem('InputValue', this.state.TextInputValue);
    } catch (e) {
      // saving error
    }
  };

  fetchData = async () => {
    try {
      this.setState({DisplayValue: await AsyncStorage.getItem('InputValue')});
    } catch (e) {
      // saving error
    }
  };

  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
        <TextInput
          style={styles.TextinputStyle}
          onChangeText={(txt) => {
            this.setState({TextInputValue: txt});
          }}
        />
        <MyButton title="Save data" btnPress={this.storeData} />
        <ActivityIndicator size="large" color="red" />
        <MyButton title="Fetch data" btnPress={this.fetchData} />
        <Text style={styles.textstyle}>{this.state.DisplayValue}</Text>
      </KeyboardAvoidingView>
    );
  }
}
