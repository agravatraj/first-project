import {StyleSheet} from 'react-native';
import {Colors} from '../../../Utils/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    padding:10
  },
  textstyle: {
    fontSize: 30,
    alignSelf: 'center',
    marginTop:10,

  },
  TextinputStyle: {
    height: 40,
    // width:50,
    borderWidth: 1,
    marginBottom:10
  },
});

export default styles;
