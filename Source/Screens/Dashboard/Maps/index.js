import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {BackButton} from '../../../Utils/svg'
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: 21.7644725,
        longitude: 72.15193040000001,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
      },
    };
    //this._getLocation();
  }

  _getLocation = () => {
    Geolocation.getCurrentPosition((info) => {
      console.log('this is latitude ', info.coords.latitude);
      console.log('this is longitude ', info.coords.longitude);
      var lat = parseFloat(info.coords.latitude);
      var long = parseFloat(info.coords.longitude);
      console.log(lat, long);

      this.setState({
        region: {
          latitude: lat,
          longitude: long,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        },
      });
    });
  };
  _leftButton=()=>(
    <View style={{alignItems:'center' ,justifyContent:'center'}}>
      <TouchableOpacity onPress={()=>{this.props.navigation.pop()}}>
        <BackButton fill="#fff" height={32} width={32} />
      </TouchableOpacity>
    </View>
  )

  render() {
    console.log('render called', this.state.region);
    return (
      <View style={styles.container}>
        <MapView
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={styles.map}
          initialRegion={this.state.region}
          region={this.state.region}
          showsUserLocation={true}
          //mapType="satellite"
        />
        <GooglePlacesAutocomplete
          placeholder="Search"
          fetchDetails={true}
          renderLeftButton={this._leftButton}
          onPress={(data, details = null) => {
            // 'details' is provided when fetchDetails = true
            console.log(
              'data',
              JSON.stringify(data, null, 2),
              'details',
              JSON.stringify(details.geometry.location.lat, null, 2),
              //details.geometry.location.lat , lat lng find karva mate
            );
            var lat = parseFloat(details.geometry.location.lat);
            var lng = parseFloat(details.geometry.location.lng);
            this.setState({
              region: {
                latitude: lat,
                longitude: lng,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              },
            });
            console.log(
              'my latitude=',
              this.state.region.latitude,
              'my longitude',
              this.state.region.longitude,
            );
          }}
          query={{
            key: 'AIzaSyCthIiAmvDhEZ1Vn3dnf-Iy4k-1_HuoTGA',
            language: 'en',
          }}
          styles={{
            container: {
              flex: 0,
              margin: 10,
              // padding:10,
              backgroundColor: 'white',
            },
            textInputContainer: {
              backgroundColor: 'black',
              borderRadius: 10,
            },
            textInput: {
              //  marginLeft: 2,
              //  marginRight: 2,
              //  marginEnd:2,
              //  marginTop:2,
              // height: 38,
              color: '#5d5d5d',
              // fontSize: 16,
            },
          }}
        />
      </View>
    );
  }
}
