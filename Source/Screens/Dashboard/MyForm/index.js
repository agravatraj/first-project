/* eslint-disable eqeqeq */
/* eslint-disable react-native/no-inline-styles */
// eslint-disable-next-line prettier/prettier
import React from  'react';
import Images from '../../../Utils/images';
import CustomeCheckBox from '../../../Component/CustomeCheckBox';
import styles from './styles';
import {GoogleIcon} from '../../../Utils/svg';
//import DropDownPicker from 'react-native-dropdown-picker';

const MY_DATA = [
  {
    label: 'India',
    value: 'India',
  },
  {
    label: 'UK',
    value: 'uk',
  },
  {
    label: 'France',
    value: 'france',
  },
  {
    label: 'Brazil',
    value: 'Brazil',
  },
  {
    label: 'Nepal',
    value: 'Nepal',
  },
];

import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  Keyboard,
  ImageBackground,
  ScrollView,
  Image,
} from 'react-native';
import CustomeButton from '../../../Component/CustomeButton';
import {Colors} from '../../../Utils/colors';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      RadioValue: '',
      Fname: '',
      Lname: '',
      Age: '',
      Addess: '',
      Submit: false,
      gender: '',
      CheckBoxValue: [],
      isTrue: false,
      Checkbox1: '',
      Checkbox2: '',
      Checkbox3: '',
      value1: false,
      value2: false,
      value3: false,
      country: 'uk',
    };
  }
  _submit = () => {
    this.setState({
      Submit: true,
      gender: this.state.RadioValue == 'm' ? 'male' : 'female',
    });
    Keyboard.dismiss();
  };
  _clear = () => {
    this.setState({
      Fname: '',
      Lname: '',
      Age: '',
      Addess: '',
      RadioValue: '',
      Submit: false,
      gender: '',
    });
  };
  render() {
    return (
      <SafeAreaView style={styles.container1}>
        <ImageBackground
          source={Images.backgroundImage}
          style={styles.backgroundImageStyle}>
          {/* <DropDownPicker
            items={MY_DATA}
            defaultValue={this.state.country}
            containerStyle={{height: 40}}
            style={{backgroundColor: '#fafafa'}}
            itemStyle={{
              justifyContent: 'flex-start',
            }}
            dropDownStyle={{backgroundColor: '#fafafa'}}
            onChangeItem={(item) =>
              this.setState({
                country: item.value,
              })
            }
            customArrowUp={() => {
              return <Image source={Images.upArrow} height={16} width={16} />;
            }}
            customArrowDown={() => {
              return <Image source={Images.downArrow} height={16} width={16} />;
            }}
          /> */}
          <ScrollView bounces={false}>
            <View style={styles.Cardstyle}>
              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>First name</Text>
                </View>
                <View style={styles.View2Style}>
                  <TextInput
                    style={styles.TextInputStyle}
                    placeholder="Enter first name"
                    placeholderTextColor={Colors.black}
                    value={this.state.Fname}
                    onChangeText={(txt) => {
                      this.setState({Fname: txt});
                    }}
                  />
                </View>
              </View>

              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>Last name</Text>
                </View>
                <View style={styles.View2Style}>
                  <TextInput
                    style={styles.TextInputStyle}
                    placeholder="Enter last name"
                    placeholderTextColor={Colors.black}
                    value={this.state.Lname}
                    onChangeText={(txt) => {
                      this.setState({Lname: txt});
                    }}
                  />
                </View>
              </View>

              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>Age</Text>
                </View>
                <View style={styles.View2Style}>
                  <TextInput
                    style={styles.TextInputStyle}
                    placeholder="Enter your age"
                    placeholderTextColor={Colors.black}
                    keyboardType="numeric"
                    value={this.state.Age}
                    onChangeText={(txt) => {
                      this.setState({Age: txt});
                    }}
                  />
                </View>
              </View>

              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>Gender</Text>
                </View>
                <View style={styles.View2Style}>
                  <View style={{flex: 1, flexDirection: 'row'}}>
                    <TouchableOpacity
                      style={styles.RadioStyle}
                      onPress={() => {
                        this.setState({RadioValue: 'm'});
                      }}>
                      <View
                        style={[
                          styles.RadioViewStyle,
                          {
                            backgroundColor:
                              this.state.RadioValue == 'm'
                                ? Colors.radioButtonColor
                                : Colors.white,
                          },
                        ]}
                      />
                    </TouchableOpacity>

                    <Text style={styles.GenderTextStyles}>Male</Text>

                    <TouchableOpacity
                      style={styles.RadioStyle}
                      onPress={() => {
                        this.setState({RadioValue: 'f'});
                      }}>
                      <View
                        style={[
                          styles.RadioViewStyle,
                          {
                            backgroundColor:
                              this.state.RadioValue == 'f'
                                ? Colors.radioButtonColor
                                : Colors.white,
                          },
                        ]}
                      />
                    </TouchableOpacity>

                    <Text style={styles.GenderTextStyles}>Female</Text>
                  </View>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>Hobbies</Text>
                </View>
                <View style={styles.View2Style}>
                  <View style={styles.CheckBoxViewStyle}>
                    <CustomeCheckBox
                      lable="Playing Cricket"
                      onPress={() => {
                        this.setState({
                          Checkbox1: 'playing cricket',
                          value1: !this.state.value1,
                        });
                      }}
                      value={this.state.value1}
                    />
                  </View>
                  <View style={styles.CheckBoxViewStyle}>
                    <CustomeCheckBox
                      lable="Reading books"
                      onPress={() => {
                        this.setState({
                          Checkbox2: 'Reading books',
                          value2: !this.state.value2,
                        });
                      }}
                      value={this.state.value2}
                    />
                  </View>
                  <View style={styles.CheckBoxViewStyle}>
                    <CustomeCheckBox
                      lable="Traveling"
                      onPress={() => {
                        this.setState({
                          Checkbox3: 'Traveling',
                          value3: !this.state.value3,
                        });
                      }}
                      value={this.state.value3}
                    />
                  </View>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>Address</Text>
                </View>
                <View style={styles.View2Style}>
                  <TextInput
                    style={styles.TextInputStyle}
                    placeholder="Enter your address"
                    placeholderTextColor={Colors.black}
                    multiline={true}
                    value={this.state.Addess}
                    onChangeText={(txt) => {
                      this.setState({Addess: txt});
                    }}
                  />
                </View>
              </View>
              <View style={styles.container2}>
                <View
                  style={[
                    styles.View1Style,
                    {alignItems: 'flex-end', marginRight: 10, marginTop: 30},
                  ]}>
                  <CustomeButton
                    buttonColor={Colors.black}
                    title="Submit1"
                    btnPress={this._submit}
                  />
                </View>
                <View
                  style={[
                    styles.View2Style,
                    {alignItems: 'flex-start', marginLeft: 10, marginTop: 30},
                  ]}>
                  <CustomeButton
                    title="clear"
                    buttonColor={Colors.red}
                    btnPress={this._clear}
                  />
                  <CustomeButton
                    title="Submit2"
                    buttonColor={Colors.red}
                    btnPress={() => {
                      this.props.navigation.navigate('FormDetails', {
                        Firstname: this.state.Fname,
                        Lastname: this.state.Lname,
                        Age: this.state.Age,
                        Gender: this.state.RadioValue,
                        Address: this.state.Addess,
                        // Hobbies:
                        //   this.state.value1 ? this.state.Checkbox1 : '' +
                        //   this.state.value2 ? this.state.Checkbox2 : '' +
                        //   this.state.value3 ? this.state.Checkbox3 : ''
                      });
                    }}
                  />
                </View>
              </View>
            </View>
            <View style={styles.DisplayViewStyle}>
              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>First name</Text>
                </View>
                <View style={styles.View2Style}>
                  <Text style={styles.TextStyle}>
                    {this.state.Submit == true ? this.state.Fname : ''}
                  </Text>
                </View>
              </View>

              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>Last name</Text>
                </View>
                <View style={styles.View2Style}>
                  <Text style={styles.TextStyle}>
                    {this.state.Submit == true ? this.state.Lname : ''}
                  </Text>
                </View>
              </View>

              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>Age</Text>
                </View>
                <View style={styles.View2Style}>
                  <Text style={styles.TextStyle}>
                    {this.state.Submit == true ? this.state.Age : ''}
                  </Text>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>Gender</Text>
                </View>
                <View style={styles.View2Style}>
                  <Text style={styles.TextStyle}>{this.state.gender}</Text>
                </View>
              </View>

              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>Address</Text>
                </View>
                <View style={styles.View2Style}>
                  <Text style={styles.TextStyle}>
                    {this.state.Submit == true ? this.state.Addess : ''}
                  </Text>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.View1Style}>
                  <Text style={styles.TextStyle}>Hobbies</Text>
                </View>
                <GoogleIcon fill="#ff0000" />
                <View style={styles.View2Style}>
                  <Text style={styles.TextStyle}>
                    {this.state.value1 ? this.state.Checkbox1 : ''}
                    {'\n'}
                    {this.state.value2 ? this.state.Checkbox2 : ''}
                    {'\n'}
                    {this.state.value3 ? this.state.Checkbox3 : ''}
                  </Text>
                </View>
              </View>
            </View>
          </ScrollView>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}
