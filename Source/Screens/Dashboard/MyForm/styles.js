import {StyleSheet} from 'react-native';
import {Colors} from '../../../Utils/colors';
const Styles = StyleSheet.create({
  container1: {
    flex: 1,
    //alignItems: 'center',
    //padding: 10,
    backgroundColor: Colors.white,
    //justifyContent: 'center',
  },
  container2: {
    //flex: 1,
    flexDirection: 'row',
  },
  View1Style: {
    flex: 1,
    paddingLeft: 20,
    justifyContent: 'center',
  },
  View2Style: {
    flex: 1,
    justifyContent: 'center',
    paddingVertical: 3,
    paddingRight: 20,
  },
  TextInputStyle: {
    height: 40,
    paddingStart: 5,
    borderBottomWidth: 1,
    backgroundColor: Colors.my_background,
    fontSize: 15,
    paddingEnd: 5,
  },
  TextStyle: {
    fontSize: 20,
    color: Colors.white,
  },
  RadioStyle: {
    width: 20,
    height: 20,
    backgroundColor: Colors.white,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
  },
  RadioViewStyle: {
    height: 10,
    width: 10,
    borderRadius: 5,
  },
  GenderTextStyles: {
    fontSize: 16,
    marginHorizontal: 5,
    color: Colors.black,
  },

  Cardstyle: {
    marginHorizontal: 40,
    borderRadius: 10,
    paddingVertical: 50,
    backgroundColor: Colors.my_background,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.7,
    shadowRadius: 7.49,
  },
  DisplayViewStyle: {
    marginHorizontal: 40,
    borderRadius: 10,
    paddingVertical: 50,
    backgroundColor: Colors.black,
    marginVertical: 20,
  },
  backgroundImageStyle: {
    flex: 1,
    justifyContent: 'center',
    paddingVertical: 5,
  },
  CheckBoxViewStyle: {
    flexDirection: 'row',
    paddingVertical: 5,
  },
});

export default Styles;
