import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';
import MyButton from '../../../Component/CustomeButton';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isTrue: false};
  }
  render() {
    const {
      Firstname,
      Lastname,
      Age,
      Gender,
      Address,
      Hobbies,
    } = this.props.route.params;
    return (
      <View style={styles.container}>
        <View style={styles.DisplayViewStyle}>
          <View style={styles.container2}>
            <View style={styles.View1Style}>
              <Text style={styles.TextStyle}>First name</Text>
            </View>
            <View style={styles.View2Style}>
              <Text style={styles.TextStyle}>{Firstname}</Text>
            </View>
          </View>

          <View style={styles.container2}>
            <View style={styles.View1Style}>
              <Text style={styles.TextStyle}>Last name</Text>
            </View>
            <View style={styles.View2Style}>
              <Text style={styles.TextStyle}>{Lastname}</Text>
            </View>
          </View>

          <View style={styles.container2}>
            <View style={styles.View1Style}>
              <Text style={styles.TextStyle}>Age</Text>
            </View>
            <View style={styles.View2Style}>
              <Text style={styles.TextStyle}>{Age}</Text>
            </View>
          </View>
          <View style={styles.container2}>
            <View style={styles.View1Style}>
              <Text style={styles.TextStyle}>Gender</Text>
            </View>
            <View style={styles.View2Style}>
              <Text style={styles.TextStyle}>
                {Gender == 'm' ? 'male' : 'female'}
              </Text>
            </View>
          </View>

          <View style={styles.container2}>
            <View style={styles.View1Style}>
              <Text style={styles.TextStyle}>Address</Text>
            </View>
            <View style={styles.View2Style}>
              <Text style={styles.TextStyle}>{Address}</Text>
            </View>
          </View>
          <View style={styles.container2}>
            <View style={styles.View1Style}>
              <Text style={styles.TextStyle}>Hobbies</Text>
            </View>
            <View style={styles.View2Style}>
              <Text style={styles.TextStyle}>{Hobbies}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
