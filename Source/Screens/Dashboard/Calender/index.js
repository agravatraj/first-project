import React from 'react';
import {View} from 'react-native';
import styles from './styles';
import MyButton from '../../../Component/CustomeButton';
import {Calendar} from 'react-native-calendars';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isTrue: false};
  }
  showCalender = () => {
    this.setState({isTrue: !this.state.isTrue});
  };
  render() {
    return (
      <View style={styles.container}>
        <MyButton
          title="Calender"
          buttonColor="blue"
          btnPress={this.showCalender}
        />
        {this.state.isTrue ? <Calendar /> : <></>}
      </View>
    );
  }
}
