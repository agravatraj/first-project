import React from 'react';
import {  View, Text, Alert } from 'react-native';
import styles from './styles'
import CustomeButton from '../../../Component/CustomeButton'
import {callApi} from '../../../Api/NetworkCall'
import API_CONFIG from '../../../Api/ApiConfig'
import Loader from '../../../Component/Loader'
import axios from 'react-native-axios'
import AsyncStorage from '@react-native-community/async-storage';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        loading:false,
        isData:false,
    };
  }

 componentDidMount() {
   var myresponse= this.getItem()
   console.log("my response",myresponse);
   if(myresponse!=null)
   {
    console.log("if called");
     this.setState({isData:true});
   }
  }

  render() {
    return (
      <View style={styles.container}>
          {this.state.loading ? <Loader is_back_deam={true} /> : null}
        <CustomeButton
          title="Get"
          buttonColor="dodgerblue"
          btnPress={() => {
            this.getData()
          }}
        />
          <CustomeButton
          title="Post"
          buttonColor="dodgerblue"
          btnPress={() => {
            this.postData()
          }}
        />
        <CustomeButton
          title={this.state.isData?"Logout":"Login"} 
          buttonColor="red"
          btnPress={this.state.isData? ()=>{this.Clear()} : () => {
            this.Login()
          }}
        />
      </View>
    );
  }
 async Clear()
  {
    console.log("clear called")
   await AsyncStorage.clear()
   this.setState({isData:false})
  }
 async setItem(response)
  {
    console.log("thisis response",response);
    await AsyncStorage.setItem('UserData',JSON.stringify(response.data))
    this.setState({isData:true})
  }
  async getItem()
  {
   var mydata= await AsyncStorage.getItem('UserData')
   console.log("my async storage data==",mydata)
   return mydata;
  }
  async Login(){
    console.log("Login button pressed-------")
    this.setState({loading: true})
    await axios.post('https://local.puppilovers.com/api/user/login',
    {
      withCredentials: false,
       headers:{
        'Content-Type': 'application/json'
       }
       
    },{
      auth: {username: '6070809000',password: '123456'},
      data:{
        "countryCode": "61"
      }
    }).then(response=>{
      if(response.data.success===true){
        this.setState({loading:false})
        try{
          this.setItem(response)
        }
        catch(error){
          console.log("errror in setItem",error)
        }
      }
      console.log("my response",JSON.stringify(response,null,2))
    })
  }

  async getData(){
    console.log("get data called-------")
    this.setState({loading: true})
    const response= await callApi(
        'https://local.puppilovers.com/api/user/check/61/9106793452?type=login',
        null,
        API_CONFIG.GET,
        null
    )
    if(response.body!=null)
    {
        console.log("my response=",JSON.stringify(response.body,null,2))
        if(response.body.status===200)
        {
            this.setState({loading:false})
        }
        else{
            this.setState({loading:false},()=>{setTimeout(()=>{
                Alert.alert("Something went wrong")
            },10)})
        }
    }
    else{
        this.setState({loading:false},()=>{setTimeout(()=>{
            Alert.alert("did not get response check url")
        },10)})
    }
  }
  async postData(){
    this.setState({loading:true})
    const response= await callApi(
        'https://postman-echo.com/post',
        [{foo1:'bar1'},{foo2:'bar2'}],
        API_CONFIG.POST,
        null
    )
    if(response !=null)
    {
        console.log("my response=",JSON.stringify(response.body,null,2))
        if(response.body.status===200)
        {
            this.setState({loading:false})
           
        }
        else{
            this.setState({loading:false},()=>{setTimeout(()=>{
                Alert.alert("Something went wrong")
            },10)})
        }
    }
    else{
        this.setState({loading:false},()=>{setTimeout(()=>{
            Alert.alert("did not get response in post")
        },10)})
    }
  }
}
