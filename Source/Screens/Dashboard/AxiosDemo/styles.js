import {StyleSheet} from 'react-native';
import {Colors} from '../../../Utils/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: Colors.grey,
  },
});

export default styles;
