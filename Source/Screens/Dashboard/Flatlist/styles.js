import {StyleSheet} from 'react-native';
import {Colors} from '../../../Utils/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flstyle: {
    backgroundColor: Colors.flatlistbackground,
  },
  fontStyle: {
    fontSize: 30,
    color: Colors.white,
    alignSelf: 'center',
  },
  devider: {
    height: 1,
    backgroundColor: Colors.white,
  },
  indicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
