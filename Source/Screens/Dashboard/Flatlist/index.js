import React from 'react';
import {
  View,
  Text,
  FlatList,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import axios from 'axios';
import MyActivityIndicator from '../../../Component/CustomeActivityIndicator';

const renderItem = ({item}) => (
  <View style={styles.flstyle}>
    <Text style={styles.fontStyle}>{item.name}</Text>
    <View style={styles.devider} />
  </View>
);

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loading: true,
      my_data: [],
    };
    //this._callApiFromAxios();
    this._callApiFromFetch();
  }
  _callApiFromAxios = async () => {
    await axios
      .get('https://jsonplaceholder.typicode.com/users')
      .then((response) => {
        //console.log('Names', JSON.stringify(response.data.name, null, 2));
        let Data = [];
        response.data.map((item) => {
          //console.log('Names', JSON.stringify(item.name, null, 2));
          Data.push({name: item.name});
        });
        console.log('myData', Data);
        this.setState({my_data: Data, refreshing: false, loading: false});
      })
      .catch((error) => {
        console.log('uybub', error);
      });
  };
  _callApiFromFetch = async () => {
    try {
      await fetch('https://jsonplaceholder.typicode.com/users')
        .then((response) => response.json())
        .then((response) => {
          console.log('myresponse=>', JSON.stringify(response, null, 2));
          let Data = [];

          response.map((item) => {
            Data.push({id: item.id, name: item.name});
          });
          console.log('myData', Data);
          this.setState({my_data: Data, refreshing: false, loading: false});
        })
        .catch((error) => {
          console.log('this is error', error);
        });
    } catch (error) {}
  };
  _onRefresh = () => {
    this.setState(
      {
        refreshing: true,
      },
      () => {
        this._callApiFromFetch();
      },
    );
  };
  render() {
    console.log('render method called');
    return (
      <View style={styles.container}>
        {this.state.loading ? (
          <MyActivityIndicator size="large" color="blue" />
        ) : (
          <FlatList
            data={this.state.my_data}
            renderItem={renderItem}
            keyExtractor={(item) => item.id + ''}
            //numColumns={2}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          />
        )}
        {/* <Text>rajjj</Text>
       <ActivityIndicator size='large' color='black'/> */}
      </View>
    );
  }
}
