import React from 'react';
import {
  View,
  Text,
  //Modal,
  Button,
  TouchableHighlight,
  Alert,
  ScrollView
} from 'react-native';
import Modal from 'react-native-modal'
import styles from './styles';
import MyButton from '../../../Component/CustomeButton';


export default class index extends React.Component {
  constructor(props) {
    super(props,{      scrollOffset: null});
    this.scrollViewRef = React.createRef();
    this.state = {
      modalVisible: false,
    };
  }

  handleOnScroll = event => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  };
  handleScrollTo = p => {
    if (this.scrollViewRef.current) {
      this.scrollViewRef.current.scrollTo(p);
    }
  };

  setModalVisible = (visible) => {
    this.setState({modalVisible: visible});
  };

  render() {
    const {modalVisible} = this.state;
    return (
      <View style={styles.container1}>
        {/* simple Modal from react-native */}
        {/* <Button
          title="show modal"
          onPress={() => {
            this.setModalVisible(true);
          }}
        />
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>hi i am modal </Text>

              <MyButton
                title="hide modal"
                btnPress={() => {
                  this.setModalVisible(!modalVisible);
                }}
              />
            </View>
          </View> ̰
        </Modal> */}

        <Button title="Show modal" onPress={()=>{this.setModalVisible(true)}} />

        {/* <Modal 
          isVisible={modalVisible} 
          animationIn='bounceInLeft' 
          animationOut='slideOutLeft' 
          animationOutTiming={800} 
          animationInTiming={800} 
          onBackButtonPress={()=>{this.setModalVisible(!modalVisible)}}
          onBackdropPress={()=>{this.setModalVisible(!modalVisible)}}
          swipeDirection='left'
          swipeThreshold={2000}
          //onSwipeStart={()=>{this.setModalVisible(!modalVisible)}}
          style={{justifyContent:'center',alignItems:"center",flex:1}}>

          <View style={{height:200,width:300,borderRadius:20, backgroundColor:"white",padding:20}}>
            <Text>Hello!</Text>

            <Button title="Hide modal" onPress={()=>{this.setModalVisible(!modalVisible)}} />
          </View>

        </Modal> */}

        <Modal
        testID={'modal'}
        isVisible={modalVisible}
        onSwipeComplete={()=>{this.setModalVisible(!modalVisible)}}
        swipeDirection={['down']}
        scrollTo={this.handleScrollTo}
        scrollOffset={this.state.scrollOffset}
        scrollOffsetMax={400 - 300} // content height - ScrollView height
        propagateSwipe={true}
        style={styles.modal}
        animationOutTiming={1000}
        backdropTransitionInTiming={0}
        backdropTransitionOutTiming={0}
        onBackdropPress={()=>{this.setModalVisible(!modalVisible)}}>
        
        <View style={styles.scrollableModal}>
          <ScrollView
            ref={this.scrollViewRef}
            onScroll={this.handleOnScroll}
            scrollEventThrottle={16}>
            <View style={styles.scrollableModalContent1}>
              <Text style={styles.scrollableModalText1}>
                You can scroll me up! 👆
              </Text>
            </View>
            <View style={styles.scrollableModalContent2}>
              <Text style={styles.scrollableModalText2}>
                Same here as well! ☝
              </Text>
            </View>
          </ScrollView>
        </View>
      </Modal>
      </View>
    );
  }
}
