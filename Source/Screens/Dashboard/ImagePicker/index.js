import React from 'react';
import {
  Text,
  View,
  Image
} from 'react-native';

import styles from './styles'
import Mybutton from '../../../Component/CustomeButton'
import ImagePicker from 'react-native-image-picker';
import Images from '../../../Utils/images'

const options = {
  title: 'Select Image',
  //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      gettingLoginStatus: true,
      avatarSource:null
    };
  }

  _showImage=()=>{
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
     
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
     
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
     
        this.setState({
          avatarSource: source,
        });
      }
    });
  }  
  render() {
        return (
          <View style={styles.container}>
            <Image style={{height:300,width:300}} source={this.state.avatarSource} />
            <Mybutton title="Choose Image" buttonColor="dodgerblue" btnPress={this._showImage} />
          </View>
        );
  }
}