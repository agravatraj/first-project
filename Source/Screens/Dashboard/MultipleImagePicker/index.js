import React from 'react';
import {
  Text,
  View,
  Image
} from 'react-native';

import styles from './styles'
import Mybutton from '../../../Component/CustomeButton'
import ImagePicker from 'react-native-image-crop-picker';
import Images from '../../../Utils/images'
import { multiply } from 'react-native-reanimated';
import { FlatList } from 'react-native-gesture-handler';



export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      gettingLoginStatus: true,
      imageSource:[]
    };
  }

  _showImage=()=>{
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      multiple: true
    }).then(image => {
      console.log(image);
      let ImageArray=[];
      image.map((item)=>{ImageArray.push({path:item.path})})
      // const source={uri:image.path}
       console.log("images paths==>",ImageArray)
       this.setState({imageSource:ImageArray})
    });
    
   
  }  
  render() {
    console.log("my paths ==>",this.state.imageSource)
        return (
          <View style={styles.container}>
            <FlatList 
              data={this.state.imageSource}
              renderItem={this.renderItem}
              keyExtractor={(item)=>item.path+''}
            />
            {/* <Image style={{height:300,width:300}} source={this.state.imageSource} /> */}
            <Mybutton title="Choose Image" buttonColor="dodgerblue" btnPress={this._showImage} />
          </View>
        );
  }
   renderItem = ({item}) => {
     console.log("item",item)
    return(<View>
      <Image style={{height:300,width:300}} source={{uri:item.path}} />
    </View>);
   }
}