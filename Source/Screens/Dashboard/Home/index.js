import React from 'react';
import {View} from 'react-native';
import styles from './styles';
import MyButton from '../../../Component/CustomeButton';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isTrue: false};
  }
  render() {
    return (
      <View style={styles.container1}>
        <MyButton
          title="Myform"
          buttonColor="blue"
          btnPress={() => {
            this.props.navigation.navigate('MyForm');
          }}
        />
        <MyButton
          title="Calender"
          buttonColor="blue"
          btnPress={() => {
            this.props.navigation.navigate('Calender');
          }}
        />
        <MyButton
          title="Flatlist"
          buttonColor="blue"
          btnPress={() => {
            this.props.navigation.navigate('Flatlist');
          }}
        />
        <MyButton
          title="AsyncStorage"
          buttonColor="blue"
          btnPress={() => {
            this.props.navigation.navigate('AsyncStorage');
          }}
        />
        <MyButton
          title="Modal"
          buttonColor="blue"
          btnPress={() => {
            this.props.navigation.navigate('Modal');
          }}
        />
        <MyButton
          title="Map"
          buttonColor="blue"
          btnPress={() => {
            this.props.navigation.navigate('Map');
          }}
        />
        <MyButton
          title="Google Sign in with firebase"
          buttonColor="red"
          btnPress={() => {
            this.props.navigation.navigate('Firebase');
          }}
        />
        <MyButton
          title="Image Picker"
          buttonColor="grey"
          btnPress={() => {
            this.props.navigation.navigate('ImagePicker');
          }}
        />
         <MyButton
          title="Multiple Image Picker"
          buttonColor="purple"
          btnPress={() => {
            this.props.navigation.navigate('MultipleImagePicker');
          }}
        />
          <MyButton
          title="Axios demo"
          buttonColor="dodgerblue"
          btnPress={() => {
            this.props.navigation.navigate('AxiosDemoScreen');
          }}
        />
      </View>
    );
  }
}
