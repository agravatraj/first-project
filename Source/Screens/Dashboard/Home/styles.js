import {StyleSheet} from 'react-native';
import {Colors} from '../../../Utils/colors';
const Styles = StyleSheet.create({
  container1: {
    flex: 1,
    //alignItems: 'center',
    padding: 10,
    backgroundColor: Colors.white,
    justifyContent: 'center',
  },
});

export default Styles;
