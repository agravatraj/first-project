 import API_CONFIG from './ApiConfig';
import axios from 'react-native-axios';

export const callApi = (path, body, method, token) => {
  const headers = {
    'Content-Type': 'application/json',
  };
  if (token != null) {
    headers['Authorization'] = token;
  }
  const url = path;
  // const url = [API_CONFIG.BASE_URL, path].join('/');
  if (method == API_CONFIG.GET) {
    return axios
      .get(url, {headers})
      .then(response => {
        return {body: response};
      })
      .catch(error => {
        const errorJson = error.response;
        return {body: errorJson};
      });
  }
  if (method == API_CONFIG.GET_WITH_DATA) {
    return axios
      .get(url, body, {headers})
      .then(response => {
        return {body: response};
      })
      .catch(error => {
        const errorJson = error.response;
        return {body: errorJson};
      });
  } else if (method == API_CONFIG.POST) {
    return axios
      .post(url, body, {headers})
      .then(response => {
        return {body: response};
      })
      .catch(error => {
        const errorJson = error.response;
        return {body: errorJson};
      });
  } else if (method == API_CONFIG.PUT) {
    return axios
      .put(url, body, {headers})
      .then(response => {
        return {body: response};
      })
      .catch(error => {
        const errorJson = error.response;
        return {body: errorJson};
      });
  } else if (method == API_CONFIG.DELETE) {
    return axios
      .delete(url, {headers})
      .then(response => {
        return {body: response};
      })
      .catch(error => {
        const errorJson = error.response;
        return {body: errorJson};
      });
  } else if (method == API_CONFIG.DELETE_WITH_BODY) {
    return axios
      .delete(url, {data: body, headers})
      .then(response => {
        return {body: response};
      })
      .catch(error => {
        const errorJson = error.response;
        return {body: errorJson};
      });
  } else if (method == API_CONFIG.IMAGE_UPLOAD) {
    try {
      const headers = {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      };
      return axios
        .put(url, body, {headers})
        .then(response => {
          const responseJson = JSON.stringify(response);
          return {body: response};
        })
        .catch(error => {
          const errorJson = error.response;
          return {body: errorJson};
        });
    } catch (e) {}
  } else if (method == API_CONFIG.ARTICLE_IMAGE_UPLOAD) {
    try {
      const headers = {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      };
      return axios
        .post(url, body, {headers})
        .then(response => {
          const responseJson = JSON.stringify(response);
          return {body: response};
        })
        .catch(error => {
          const errorJson = error.response;
          return {body: errorJson};
        });
    } catch (e) {}
  } else if (method == API_CONFIG.PLACE_API) {
    return axios
      .get(path)
      .then(response => {
        return {body: response};
      })
      .catch(error => {
        const errorJson = error.response;
        return {body: errorJson};
      });
  }
};
