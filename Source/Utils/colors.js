export const Colors = {
  white: '#FFFFFF',
  black: '#000000',
  transparant: '#00000000',
  red: '#ff0000',
  my_background: 'rgba(52, 52, 52, 0.4)',
  radioButtonColor: '#000000',
  flatlistbackground: '#4863A0',
  statusbar_modal_clr: 'rgba(0, 0, 0, 0.5)',
  theme: '#003E51',
  progress_back_color: 'rgba(0, 0, 0, 0.5)',
};
