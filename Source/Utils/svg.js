import React from 'react';
import {Svg, Defs, Path} from 'react-native-svg';
import {Colors} from './colors' 

export const GoogleIcon = (props) => (
  <Svg width={24.361} height={25} viewBox="0 0 24.361 25" {...props}>
    <Defs />
    <Path
      fill="#FFFFFF"
      {...props}
      className="a"
      d="M74.757,263.619a12.5,12.5,0,1,1,0-25,11.906,11.906,0,0,1,8.567,3.443l-2.411,2.41a8.7,8.7,0,0,0-6.156-2.439,9.084,9.084,0,0,0,0,18.166,8.369,8.369,0,0,0,6.313-2.5,7.138,7.138,0,0,0,1.861-4.3H74.757v-3.414H86.251a11.048,11.048,0,0,1,.185,2.136,11.3,11.3,0,0,1-2.955,7.989,11.418,11.418,0,0,1-8.723,3.509"
      transform="translate(-62.075 -238.619)"
    />
  </Svg>
);

export const BackButton=(props)=>(
  <Svg fill="#fff" height={20} viewBox="0 0 512 512" width={20} {...props}>
      <Path d="M352 128.4L319.7 96 160 256l159.7 160 32.3-32.4L224.7 256z" />
    </Svg>
)
