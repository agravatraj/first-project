const Images = {
  backgroundImage: require('../../Source/Images/tree.jpg'),
  check: require('../../Source/Images/check.png'),
  upArrow: require('../../Source/Images/uparrow.png'),
  downArrow: require('../../Source/Images/downarrow.png'),
};

export default Images;
