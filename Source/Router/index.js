import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../../Source/Screens/Dashboard/Home';
import CalenderScreen from '../../Source/Screens/Dashboard/Calender';
import FormDetailScreen from '../../Source/Screens/Dashboard/MyFormDetails';
import MyFormScreen from '../../Source/Screens/Dashboard/MyForm';
import FlatlistScreen from '../../Source/Screens/Dashboard/Flatlist';
import AsyncStorageScreen from '../../Source/Screens/Dashboard/AsyncStorage';
import ModalScreen from '../../Source/Screens/Dashboard/Modal';
import MapScreen from '../../Source/Screens/Dashboard/Maps';
import FirebaseScreen from '../../Source/Screens/Dashboard/FirebaseDemo';
import ImagePickerScreen from '../../Source/Screens/Dashboard/ImagePicker';
import MultipleImagePicker from '../../Source/Screens/Dashboard/MultipleImagePicker';
import AxiosDemoScreen from '../../Source/Screens/Dashboard/AxiosDemo' 
//import {View, Text} from 'react-native';
const Stack = createStackNavigator();

function Myfun() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home" headerMode="none">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="MyForm" component={MyFormScreen} />
        <Stack.Screen name="Calender" component={CalenderScreen} />
        <Stack.Screen name="FormDetails" component={FormDetailScreen} />
        <Stack.Screen name="Flatlist" component={FlatlistScreen} />
        <Stack.Screen name="AsyncStorage" component={AsyncStorageScreen} />
        <Stack.Screen name="Modal" component={ModalScreen} />
        <Stack.Screen name="Map" component={MapScreen} />
        <Stack.Screen name="Firebase" component={FirebaseScreen} />
        <Stack.Screen name="ImagePicker" component={ImagePickerScreen} />
        <Stack.Screen name="MultipleImagePicker" component={MultipleImagePicker} />
        <Stack.Screen name="AxiosDemoScreen" component={AxiosDemoScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default Myfun;
//Flatlist
