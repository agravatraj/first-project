import React from 'react';
import {
  View,
  ActivityIndicator,
  Modal,
  StyleSheet,
  StatusBar,
} from 'react-native';
import {Colors} from '../../Utils/colors'
import PropTypes from 'prop-types';

export default class Loader extends React.Component {
  static PropTypes = {
    is_visible: PropTypes.bool,
    is_back_deam: PropTypes.bool,
  };

  render() {
    return (
      <Modal
        animationType={'none'}
        transparent={true}
        visible={this.props.is_visible}>
        {this.props.is_back_deam ? (
          <StatusBar
            barStyle="dark-content"
            backgroundColor={Colors.statusbar_modal_clr}
          />
        ) : null}
        <View style={this.getStyle()}>
          <ActivityIndicator size="large" color={Colors.theme} />
        </View>
      </Modal>
    );
  }
  getStyle() {
    return this.props.is_back_deam
      ? styles.loader_root_view_style
      : styles.loader_root_trans_view_style;
  }
}

const styles = StyleSheet.create({
  loader_root_view_style: {
    flex: 1,
    backgroundColor: Colors.progress_back_color,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loader_root_trans_view_style: {
    flex: 1,
    backgroundColor: Colors.transparant,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
