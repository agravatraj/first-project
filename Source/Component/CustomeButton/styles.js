import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils/colors';
const Styles = StyleSheet.create({
  ButtonStyle: {
    borderWidth: 2,
    borderColor: Colors.white,
    borderRadius: 12,
    paddingHorizontal: 10,
    paddingVertical: 3,
    height: 40,
    alignItems:'center',
    justifyContent:'center'
  },
  BtnTextStyle: {
    fontSize: 18,
    color: Colors.white,
    fontWeight: 'bold',
  },
});

export default Styles;
