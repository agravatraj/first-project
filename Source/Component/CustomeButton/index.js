import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import Styles from './styles';

const CustomeButton = ({title, buttonColor='dodgerblue', borderWidth = 2, btnPress}) => {
  return (
    <TouchableOpacity
      style={[
        Styles.ButtonStyle,
        {backgroundColor: buttonColor, borderWidth: borderWidth},
      ]}
      onPress={btnPress}>
      <Text style={Styles.BtnTextStyle}>{title}</Text>
    </TouchableOpacity>
  );
};
export default CustomeButton;
