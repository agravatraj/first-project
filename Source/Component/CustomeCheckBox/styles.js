import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils/colors';
const Styles = StyleSheet.create({
  Container: {
    height: 16,
    width: 16,
    borderWidth: 1,
    backgroundColor: Colors.white,
  },
  CheckBoxTextStyle: {
    fontSize: 16,
    paddingHorizontal: 5,
  },
});

export default Styles;
