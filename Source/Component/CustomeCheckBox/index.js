/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {TouchableOpacity, Image, Text, View} from 'react-native';
import Styles from './styles';
import Images from '../../Utils/images';

const CustomeCheckbox = ({lable, onPress, value}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
      }}>
      <TouchableOpacity style={Styles.Container} onPress={onPress}>
        {value ? <Image source={Images.check} /> : <></>}
      </TouchableOpacity>
      <Text style={Styles.CheckBoxTextStyle}>{lable}</Text>
    </View>
  );
};

export default CustomeCheckbox;
