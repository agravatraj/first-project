import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import styles from './styles';
import {Colors} from '../../Utils/colors'

const CustomeIndicator = ({size="large", color="red"}) => {
  return (
    <View style={styles.container}>
      <ActivityIndicator size={size} color={color} />
    </View>
  );
};

export default CustomeIndicator;
